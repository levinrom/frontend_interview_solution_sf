var express     = require('express');
var cors        = require('cors');
var bodyParser  = require('body-parser');
var fs          = require('fs');
var app         = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

const PORT      = 8080;
const DB_FOLDER = 'db';
const DAYS      = ['sun', 'mon', 'tue', 'wed', 'thu'];

function getDay(dayId) {
    return JSON.parse(fs.readFileSync(`${DB_FOLDER}/${dayId}.json`, { encoding: 'utf-8' }));
}

app.get('/restaurants', (req, res) => {
    var restaurants = JSON.parse(fs.readFileSync(`${ DB_FOLDER }/restaurant.json`, { encoding: 'utf-8' }));
    res.send(restaurants);
})

app.get('/days', (req, res) => {
    var result = [];
    DAYS.forEach((day) => {
        var dayJson = JSON.parse(fs.readFileSync(`${DB_FOLDER}/${day}.json`, { encoding: 'utf-8' }));
        var dayResult = { "id": day, "payload": getDay(day) };
        result.push(dayResult);
    })
    res.send(result);
})

app.post('/add_eater', (req, res) => {
    var email = req.body.email;
    var name = req.body.name;
    var day = req.body.day;
    var restId = req.body.restId;

    var dayJson = getDay(day);

    dayJson[restId].push({ email, name });

    fs.writeFileSync(`${DB_FOLDER}/${day}.json`, JSON.stringify(dayJson));

    res.send('done');
})

app.listen(PORT, function () {
  console.log('Shutterfly Server on port ', PORT);
});