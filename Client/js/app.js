var SERVER = 'http://localhost:8080/';

FoodLooz = {};
FoodLooz.Models = {};
FoodLooz.Collections = {};
FoodLooz.Views = {};

// --- MODELS ---
FoodLooz.Models.Restaurant = Backbone.Model.extend({
    defaults: {
        "_type":  "Restaurant",
        "users": null
    }
})
FoodLooz.Models.Day = Backbone.Model.extend({
    defaults: {
        "_type":  "Day",
        "restaurants": null
    }
})
FoodLooz.Models.User = Backbone.Model.extend({
    defaults: {
        "_type":  "User"
    }
})

// --- COLLECTIONS ---
FoodLooz.Collections.Restaurants = Backbone.Collection.extend({
    model: FoodLooz.Models.Restaurant,

    fetch: function(callback) {
        var that = this;

        $.get(SERVER + 'restaurants', function(resp) {
            that.add(resp);

            if (_.isFunction(callback)) {
                callback();
            }
        })
    }
})

FoodLooz.Collections.Days = Backbone.Collection.extend({
    model: FoodLooz.Models.Day,

    fetch: function() {
        var that = this;
        var totalRestLeft = 5*5; // 5 days x 5 restaurants

        $.get(SERVER + 'days', function(resp) {
            // that.add(resp);
            resp.forEach(function(day) {
                var newDay = new FoodLooz.Models.Day({
                    "id": day.id
                });
                newDay.restaurants = new FoodLooz.Collections.Restaurants();
                newDay.restaurants.fetch(function() {
                    newDay.restaurants.forEach(function(rest) {
                        rest.users = new FoodLooz.Collections.Users(day.payload[rest.get('id')]);
                        totalRestLeft--;
                    });
                    that.add(newDay);
                    if (totalRestLeft == 0) {
                        that.trigger('doneLoad');
                    }
                });
            })
        })
    }
})

FoodLooz.Collections.Users = Backbone.Collection.extend({
    model: FoodLooz.Models.User
})

// --- VIEWS ---

FoodLooz.Views.AddEater = Backbone.View.extend({
    className: 'modal new-eater',
    tagName: 'div',

    events: {
        'click #eater-add-btn': 'addEater'
    },

    initialize: function(restId, dayId) {
        this.restId = restId;
        this.dayId = dayId;
    },

    render: function() {
        // Clean any others
        $('body').find('.modal').remove();
        this.$el.html(`
            <label for="eater-name">Name:</label><br/>
            <input type="text" id="eater-name" name="eater-name" />
            <br/><br/>
            <label for="eater-email">Email:</label><br/>
            <input type="text" id="eater-email" name="eater-email" />
            <br/><br/>
            <input type="button" id="eater-add-btn" value="Done" />
        `);
        return this;
    },

    addEater: function() {
        var name = this.$el.find('#eater-name').val();
        var email = this.$el.find('#eater-email').val();

        $.post(SERVER + 'add_eater', { name: name, email: email, day: this.dayId, restId: this.restId }, function(resp) {
            $('body').find('.modal').remove();
            App.render();
        })
    }
})

FoodLooz.Views.RestaurantItem = Backbone.View.extend({
    className: 'item',
    tagName: 'div',

    events: {
        'click .item-add': 'addToRestaurant'
    },

    initialize: function(model, dayId) {
        this.model = model;
        this.dayId = dayId;
    },

    render: function() {
        var restName = this.model.get('name');
        var users = this.model.users;

        this.$el.html(this._template(restName, users.length));

        return this;
    },

    addToRestaurant: function() {
        var modal = new FoodLooz.Views.AddEater(this.model.get('id'), this.dayId);
        $('body').append(modal.render().el);
    },

    _template: function(restName, totalUsers) {
        var base = _.template('<span class="item-name"><%= name %></span><span class="item-count"><%= count %></span><span class="item-add">+</span>');
        return base({ name: restName, count: totalUsers });
    }
})

FoodLooz.Views.Day = Backbone.View.extend({
    className: 'day',
    tagName: 'div',

    initialize: function(model) {
        this.model = model;
    },

    render: function() {
        var that = this;
        var dayName = this.model.get('id');

        this.$el.html(this._template(dayName));
        var restContainer = this.$el.children('.rest_container');

        this.model.restaurants.forEach(function(rest) {
            var restView = new FoodLooz.Views.RestaurantItem(rest, dayName);
            restContainer.append(restView.render().el);
        });

        return this;
    },

    _template: function(dayName) {
        var base = _.template('<h2><%= name %></h2><div class="rest_container"></div>');
        return base({ name: dayName });
    }
})

FoodLooz.Views.Main = Backbone.View.extend({
    el: $('#app'),

    initialize: function() {
        
    },

    render: function() {
        var that = this;

        this.$el.empty();

        this.days = new FoodLooz.Collections.Days();
        this.days.on('doneLoad', function() {
            that.days.forEach(function(day) {
                var dayView = new FoodLooz.Views.Day(day);
                that.$el.append(dayView.render().el);
            })
        })
        this.days.fetch();

        return this;
    }
})

var App = new FoodLooz.Views.Main;
App.render();

